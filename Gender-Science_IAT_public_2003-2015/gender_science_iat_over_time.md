# Have Gender-Science IAT scores changed over time?
Nick Michalak  
August 3, 2016  

# load required packages


```r
want_packages <- c("dplyr","Hmisc","psych","knitr")
have_packages   <- want_packages %in% rownames(installed.packages())
if(any(!have_packages)) install.packages(want_packages[!have_packages])
library(dplyr)
library(Hmisc)
library(psych)
library(knitr)
```

# load dataset^1^


```r
# path
gsiat.03.15.path <- "~/Desktop/stereotype-threat-cross-temporal-meta-analysis/Gender-Science_IAT_public_2003-2015/Gender-Science IAT.public.2003-2015.sav"

# read data
gsiat.03.15 <- spss.get(file = gsiat.03.15.path,
                        lowernames = T,
                        use.value.labels = F,
                        to.data.frame = T,
                        datevars = "date")
```

```
## Warning in read.spss(file, use.value.labels = use.value.labels,
## to.data.frame = to.data.frame, : ~/Desktop/stereotype-threat-cross-
## temporal-meta-analysis/Gender-Science_IAT_public_2003-2015/Gender-
## Science IAT.public.2003-2015.sav: Unrecognized record type 7, subtype 18
## encountered in system file
```

```
## re-encoding from CP1252
```

# descriptives of relevant vars
> note: I grab all the vars I want and then I omit NAs


```r
options(scipen=999)

gsiat.03.15 %>% dplyr::select(year, sex, d.biep.male.science.all) %>% na.omit(.) %>% dplyr::select(sex) %>% table()
```

```
## .
##             .      2      4      f      m      n 
##   5892  61794      1      0 373059 180702  23425
```

```r
# sex
gsiat.03.15 %>% dplyr::select(year, sex, d.biep.male.science.all) %>% na.omit(.) %>% mutate(sex = ifelse(sex == "m", "Male", ifelse(sex == "f", "Female", ifelse(sex == "n", "N", ifelse(sex == 2, 2, ifelse(sex == 4, 4, ifelse(sex == ".", ".", NA))))))) %>% group_by(sex) %>% summarise(n = n()) %>% mutate(prop = n/sum(n))
```

```
## # A tibble: 6 × 3
##      sex      n           prop
##    <chr>  <int>          <dbl>
## 1      .  61794 0.095823518739
## 2      2      1 0.000001550693
## 3 Female 373059 0.578499952704
## 4   Male 180702 0.280213313319
## 5      N  23425 0.036324981818
## 6   <NA>   5892 0.009136682727
```

```r
# Gender-Science IAT scores
gsiat.03.15 %>% dplyr::select(year, sex, d.biep.male.science.all) %>% na.omit(.) %>% dplyr::select(d.biep.male.science.all) %>% describe()
```

```
##                         vars      n mean  sd median trimmed mad   min  max
## d.biep.male.science.all    1 644873 0.34 0.4   0.36    0.35 0.4 -1.82 1.85
##                         range  skew kurtosis se
## d.biep.male.science.all  3.67 -0.41     0.43  0
```

```r
# Gender-Science IAT scores by year and sex
gsiat.03.15 %>% dplyr::select(year, sex, d.biep.male.science.all) %>% mutate(sex = ifelse(sex == "m", "Male", ifelse(sex == "f", "Female", ifelse(sex == "n", "N", ifelse(sex == 2, 2, ifelse(sex == 4, 4, ifelse(sex == ".", ".", NA))))))) %>% na.omit(.) %>% filter(sex == "Male" | sex == "Female") %>% group_by(year, sex) %>% summarise(n = n(),
                                  mean = mean(d.biep.male.science.all),
                                  sd = sd(d.biep.male.science.all)) %>% mutate(prop = n/sum(n)) %>% print(n = 100)
```

```
## Source: local data frame [26 x 6]
## Groups: year [13]
## 
##              year    sex     n      mean        sd      prop
##    <S3: labelled>  <chr> <int>     <dbl>     <dbl>     <dbl>
## 1            2003 Female 15810 0.3835053 0.3902114 0.6737407
## 2            2003   Male  7656 0.3660110 0.3979912 0.3262593
## 3            2004 Female 23121 0.3725772 0.3673417 0.6939284
## 4            2004   Male 10198 0.3202667 0.3961399 0.3060716
## 5            2005 Female 40141 0.3477503 0.3836351 0.6758199
## 6            2005   Male 19255 0.3307541 0.3929312 0.3241801
## 7            2006 Female 26014 0.3356934 0.3970092 0.6524705
## 8            2006   Male 13856 0.3601141 0.4020636 0.3475295
## 9            2007 Female 26363 0.3660869 0.3986075 0.6929244
## 10           2007   Male 11683 0.3890269 0.4011724 0.3070756
## 11           2008 Female 23217 0.3569034 0.3974025 0.6818903
## 12           2008   Male 10831 0.3808442 0.3996473 0.3181097
## 13           2009 Female 26815 0.3568952 0.3931934 0.6762073
## 14           2009   Male 12840 0.3821880 0.4050658 0.3237927
## 15           2010 Female 24789 0.3529035 0.3978972 0.6919277
## 16           2010   Male 11037 0.3781943 0.4058449 0.3080723
## 17           2011 Female 25923 0.3451486 0.3999647 0.6985260
## 18           2011   Male 11188 0.3688362 0.4104637 0.3014740
## 19           2012 Female 30581 0.3399736 0.4063522 0.6757187
## 20           2012   Male 14676 0.3856625 0.4093454 0.3242813
## 21           2013 Female 29470 0.3221896 0.4109647 0.6728003
## 22           2013   Male 14332 0.3755525 0.4113149 0.3271997
## 23           2014 Female 30215 0.3019463 0.4076596 0.6682517
## 24           2014   Male 15000 0.3583036 0.4098970 0.3317483
## 25           2015 Female 50600 0.2838285 0.4099274 0.6425397
## 26           2015   Male 28150 0.3675915 0.4034634 0.3574603
```

# plot


```r
yr.sex.iat.plot <- gsiat.03.15 %>% dplyr::select(year, d.biep.male.science.all, sex) %>% mutate(sex = ifelse(sex == "m", "Male", ifelse(sex == "f", "Female", ifelse(sex == "n", "N", ifelse(sex == 2, 2, ifelse(sex == 4, 4, ifelse(sex == ".", ".", NA))))))) %>% filter(sex == "Male" | sex == "Female") %>% na.omit(.) %>% mutate(year = as.integer(year)) %>% ggplot(., aes(x = year, y = d.biep.male.science.all, color = sex, group = sex)) +
  stat_summary(fun.data = "mean_se", geom = "line") +
  stat_summary(fun.data = "mean_se", geom = "point") +
  stat_summary(fun.data = "mean_se", geom = "errorbar", width = 0.1) +
  scale_x_continuous(breaks = seq(2003, 2015, 1)) +
  scale_y_continuous(breaks = seq(-.5,.5,.05), limits = c(-2,2)) +
  coord_cartesian(ylim = c(0,.5)) +
  theme_minimal() +
  scale_color_manual(values = c("red", "blue")) +
  labs(x = "Year of the session date", y = "Overall Gender-Science IAT D-score", title = "Gender-Science IAT (positive scores indicate science–male stereotyping)", color = "Participant Gender")

# print plot
yr.sex.iat.plot
```

![](gender_science_iat_over_time_files/figure-html/unnamed-chunk-4-1.png)<!-- -->

```r
# save plot
ggsave(filename = "project implicit gender-science iat by gender 2002-2015.jpeg",
       limitsize = FALSE,
       plot = yr.sex.iat.plot, path = "~/Desktop/stereotype-threat-cross-temporal-meta-analysis/Gender-Science_IAT_public_2003-2015/", height = 15, width = 30, units = "cm")
```

## explcit attitudes


```r
gsiat.03.15 %>% dplyr::select(year, ats3, sex) %>% mutate(sex = ifelse(sex == "m", "Male", ifelse(sex == "f", "Female", ifelse(sex == "n", "N", ifelse(sex == 2, 2, ifelse(sex == 4, 4, ifelse(sex == ".", ".", NA))))))) %>% filter(sex == "Male" | sex == "Female") %>% na.omit(.) %>% mutate(year = as.integer(year), ats3 = as.numeric(ats3))  %>% ggplot(., aes(x = year, y = ats3, color = sex, group = sex)) +
  stat_summary(fun.data = "mean_se", geom = "line") +
  stat_summary(fun.data = "mean_se", geom = "point") +
  stat_summary(fun.data = "mean_se", geom = "errorbar", width = 0.1) +
  scale_x_continuous(breaks = seq(2003, 2015, 1)) +
  theme_minimal() +
  scale_color_manual(values = c("red", "blue")) + labs(x = "Year of the session date", y = "-3 = Strongly disagree, 3 = Strongly agree", title = "Males perform better than females in science because of greater natural ability.", color = "Participant Gender")
```

![](gender_science_iat_over_time_files/figure-html/unnamed-chunk-5-1.png)<!-- -->

```r
gsiat.03.15 %>% dplyr::select(year, ats7, sex) %>% mutate(sex = ifelse(sex == "m", "Male", ifelse(sex == "f", "Female", ifelse(sex == "n", "N", ifelse(sex == 2, 2, ifelse(sex == 4, 4, ifelse(sex == ".", ".", NA))))))) %>% filter(sex == "Male" | sex == "Female") %>% na.omit(.) %>% mutate(year = as.integer(year), ats7 = as.numeric(ats7))  %>% ggplot(., aes(x = year, y = ats7, color = sex, group = sex)) +
  stat_summary(fun.data = "mean_se", geom = "line") +
  stat_summary(fun.data = "mean_se", geom = "point") +
  stat_summary(fun.data = "mean_se", geom = "errorbar", width = 0.1) +
  scale_x_continuous(breaks = seq(2003, 2015, 1)) +
  theme_minimal() +
  scale_color_manual(values = c("red", "blue")) + labs(x = "Year of the session date", y = "-3 = Strongly disagree, 3 = Strongly agree", title = "Males perform better than females in science because they receive greater encouragement and training.", color = "Participant Gender")
```

![](gender_science_iat_over_time_files/figure-html/unnamed-chunk-5-2.png)<!-- -->

```r
gsiat.03.15 %>% dplyr::select(year, lscience.5, sex) %>% mutate(sex = ifelse(sex == "m", "Male", ifelse(sex == "f", "Female", ifelse(sex == "n", "N", ifelse(sex == 2, 2, ifelse(sex == 4, 4, ifelse(sex == ".", ".", NA))))))) %>% filter(sex == "Male" | sex == "Female") %>% na.omit(.) %>% mutate(year = as.integer(year), lscience.5 = as.numeric(lscience.5))  %>% ggplot(., aes(x = year, y = lscience.5, color = sex, group = sex)) +
  stat_summary(fun.data = "mean_se", geom = "line") +
  stat_summary(fun.data = "mean_se", geom = "point") +
  stat_summary(fun.data = "mean_se", geom = "errorbar", width = 0.1) +
  scale_x_continuous(breaks = seq(2003, 2015, 1)) +
  theme_minimal() +
  scale_color_manual(values = c("red", "blue")) + labs(x = "Year of the session date", y = "-2 =strongly male, 0 = neither male nor female, 2 = strongly female", title = "Please rate how much you associate the following domains with males or females.: Science", color = "Participant Gender")
```

```
## Warning: Removed 2 rows containing missing values (geom_errorbar).
```

![](gender_science_iat_over_time_files/figure-html/unnamed-chunk-5-3.png)<!-- -->

```r
gsiat.03.15 %>% dplyr::select(year, lscience.7, sex) %>% mutate(sex = ifelse(sex == "m", "Male", ifelse(sex == "f", "Female", ifelse(sex == "n", "N", ifelse(sex == 2, 2, ifelse(sex == 4, 4, ifelse(sex == ".", ".", NA))))))) %>% filter(sex == "Male" | sex == "Female") %>% na.omit(.) %>% mutate(year = as.integer(year), lscience.7 = as.numeric(lscience.7))  %>% ggplot(., aes(x = year, y = lscience.7, color = sex, group = sex)) +
  stat_summary(fun.data = "mean_se", geom = "line") +
  stat_summary(fun.data = "mean_se", geom = "point") +
  stat_summary(fun.data = "mean_se", geom = "errorbar", width = 0.1) +
  scale_x_continuous(breaks = seq(2003, 2015, 1)) +
  theme_minimal() +
  scale_color_manual(values = c("red", "blue")) + labs(x = "Year of the session date", y = "1 = Strongly female, 7 = Strongly male", title = "Please rate how much you associate the following domains with males or females.: Science", color = "Participant Gender")
```

![](gender_science_iat_over_time_files/figure-html/unnamed-chunk-5-4.png)<!-- -->

# References
1. Xu, K., Lofaro, N., Nosek, B. A., & Greenwald, A. G. (2016, June 28). Gender-Science IAT 2003-2015. Retrieved from [osf.io/v9dec](osf.io/v9dec)
